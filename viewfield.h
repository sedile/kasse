#ifndef VIEWFIELD_H
#define VIEWFIELD_H

#include <QWidget>
#include <QLabel>
#include <QListWidget>

class MainFrame;

class ViewField : public QWidget {
private:
    Q_OBJECT
    QListWidget *_listview;
    QLabel *_sumLabel, *_sumCount, *_moneyLabel, *_moneyCount, *_backLabel, *_backCount, *_entry;
    MainFrame *_mainframe;

    void initComponents();
    void initSignalAndSlots();
    void initLayout();
public:
    explicit ViewField(MainFrame *mainframe, QWidget *parent = nullptr);
    virtual ~ViewField();

    void addListElement(const QString &repr);
    void removeLastElement();
    void clearList();

    void updateSum(const QString &sum);
    void updateBack(const QString &back);
    void updateEntry(const QString &number);
    void clearLabels();

    unsigned short getListSize() const;
signals:

public slots:
};

#endif // VIEWFIELD_H
