#include "mainframe.h"
#include "filehandler.h"
#include "control.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    FileHandler *fh = new FileHandler();
    Control *control = new Control(fh);
    MainFrame *frame = new MainFrame(control);
    frame->show();
    return a.exec();
}
