#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFile>

class Item;

class FileHandler : public QWidget {
private:
    Q_OBJECT

    QFile *_zettel, *_summary;
    QLabel *_artLabel, *_zetLabel, *_sumLabel;
    QLineEdit *_artPath, *_zetPath, *_sumPath;
    QPushButton *_confirm;
    QString _articlePath;

    void initComponents();
    void initSignalAndSlot();
    void initLayout();
public:
    explicit FileHandler(QWidget *parent = nullptr);
    virtual ~FileHandler();

    void saveZettel(const QList<QString> &zettel);
    QString finish(const QList<QString> &result);
    QList<Item*> getArticleList();
signals:

public slots:
    void examine();
};

#endif // FILEHANDLER_H
