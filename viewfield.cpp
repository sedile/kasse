#include <QHBoxLayout>
#include <QGroupBox>

#include "viewfield.h"

ViewField::ViewField(MainFrame *mainframe, QWidget *parent) : QWidget(parent), _mainframe(mainframe) {
    initComponents();
    initSignalAndSlots();
    initLayout();
}

ViewField::~ViewField() {}

void ViewField::initComponents(){
    _listview = new QListWidget();
    QFont font;
    font.setBold(true);
    font.setPixelSize(32);
    _listview->setFont(font);
    _sumLabel = new QLabel(tr("Zu zahlen : "));
    _sumLabel->setFixedSize(128,32);
    _sumCount = new QLabel();
    _sumCount->setFixedSize(128,32);
    _moneyLabel = new QLabel(tr("Gegeben : "));
    _moneyLabel->setFixedSize(128,32);
    _moneyCount = new QLabel();
    _moneyCount->setFixedSize(128,32);
    _backLabel = new QLabel(tr("Rueckgeld : "));
    _backLabel->setFixedSize(128,32);
    _backCount = new QLabel();
    _backCount->setFixedSize(128,32);
    _entry = new QLabel("0");
    _entry->setFixedSize(256,64);
    _entry->setFont(font);
}

void ViewField::initSignalAndSlots(){
    /* Nothing to do */
}

void ViewField::initLayout(){
    QGridLayout *layout = new QGridLayout();
    layout->addWidget(_listview, 0, 0, 1, 3);
    QGroupBox *group = new QGroupBox();
    QGridLayout *datafields = new QGridLayout();
    datafields->addWidget(_sumLabel, 0, 0);
    datafields->addWidget(_sumCount, 0, 1);
    datafields->addWidget(_moneyLabel, 1, 0);
    datafields->addWidget(_moneyCount, 1, 1);
    datafields->addWidget(_backLabel, 2, 0);
    datafields->addWidget(_backCount, 2, 1);
    group->setLayout(datafields);
    layout->addWidget(group, 1, 0, 1, 1);
    layout->addWidget(_entry, 1, 2, 1, 1);
    setLayout(layout);
    show();
}

void ViewField::addListElement(const QString &repr){
    _listview->addItem(repr);
}

void ViewField::removeLastElement(){
    delete _listview->takeItem(_listview->count() - 1);
}

void ViewField::clearList(){
    _listview->clear();
}

void ViewField::updateSum(const QString &sum){
    QString format = sum;
    if ( sum.size() > 2 && sum.at(sum.size() - 2) == '.'){
        format = format.append("0");
    }
    _sumCount->setText(format);
}

void ViewField::updateBack(const QString &back){
    QString format = back;
    if ( back.size() > 2 && back.at(back.size() - 2) == '.'){
        format = format.append("0");
    }
    _backCount->setText(format);
}

void ViewField::updateEntry(const QString &number){
    QString format = number;
    if ( number.size() > 2 && number.at(number.size() - 2) == '.'){
        format = format.append("0");
    }
    _entry->setText(format);
    _moneyCount->setText(format);
}

void ViewField::clearLabels(){
    _sumCount->setText("0.00");
    _moneyCount->setText("0.00");
    _backCount->setText("0.00");
}

unsigned short ViewField::getListSize() const {
    return _listview->count();
}
